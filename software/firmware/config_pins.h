#pragma once

#include <avr/io.h>

/* ATmega I2C */

#define PIN_I2C_SCL		0
#define PIN_I2C_SCL_DDR		DDRC
#define PIN_I2C_SCL_PORT	PORTC

#define PIN_I2C_SDA		1
#define PIN_I2C_SDA_DDR		DDRC
#define PIN_I2C_SDA_PORT	PORTC

/* ATmega SPI */

#define PIN_SPI_SCK		7
#define PIN_SPI_SCK_DDR		DDRB
#define PIN_SPI_SCK_PORT	PORTB

#define PIN_SPI_MISO		6
#define PIN_SPI_MISO_DDR	DDRB
#define PIN_SPI_MISO_PORT	PORTB
#define PIN_SPI_MISO_PIN	PINB

#define PIN_SPI_MOSI		5
#define PIN_SPI_MOSI_DDR	DDRB
#define PIN_SPI_MOSI_PORT	PORTB

#define PIN_SPI_SS		4
#define PIN_SPI_SS_DDR		DDRB
#define PIN_SPI_SS_PORT		PORTB

/* Holt HI3310 CAN Controller - SPI interface */

#define PIN_HI311X_MR		3
#define PIN_HI311X_MR_DDR	DDRB
#define PIN_HI311X_MR_PORT	PORTB

#define PIN_HI311X_INT		2
#define PIN_HI311X_INT_DDR	DDRD
#define PIN_HI311X_INT_PORT	PORTD
#define PIN_HI311X_INT_PIN	PIND

/* NXP SJA1000 CAN Controller - parallel interface */

#define PIN_SJA_AD0		0
#define PIN_SJA_AD0_DDR		DDRA
#define PIN_SJA_AD0_PORT	PORTA
#define PIN_SJA_AD0_PIN		PINA

#define PIN_SJA_AD1		1
#define PIN_SJA_AD1_DDR		DDRA
#define PIN_SJA_AD1_PORT	PORTA
#define PIN_SJA_AD1_PIN		PINA

#define PIN_SJA_AD2		2
#define PIN_SJA_AD2_DDR		DDRA
#define PIN_SJA_AD2_PORT	PORTA
#define PIN_SJA_AD2_PIN		PINA

#define PIN_SJA_AD3		3
#define PIN_SJA_AD3_DDR		DDRA
#define PIN_SJA_AD3_PORT	PORTA
#define PIN_SJA_AD3_PIN		PINA

#define PIN_SJA_AD4		4
#define PIN_SJA_AD4_DDR		DDRA
#define PIN_SJA_AD4_PORT	PORTA
#define PIN_SJA_AD4_PIN		PINA

#define PIN_SJA_AD5		5
#define PIN_SJA_AD5_DDR		DDRA
#define PIN_SJA_AD5_PORT	PORTA
#define PIN_SJA_AD5_PIN		PINA

#define PIN_SJA_AD6		6
#define PIN_SJA_AD6_DDR		DDRA
#define PIN_SJA_AD6_PORT	PORTA
#define PIN_SJA_AD6_PIN		PINA

#define PIN_SJA_AD7		7
#define PIN_SJA_AD7_DDR		DDRA
#define PIN_SJA_AD7_PORT	PORTA
#define PIN_SJA_AD7_PIN		PINA

#define PIN_SJA_CS		2
#define PIN_SJA_CS_DDR		DDRC
#define PIN_SJA_CS_PORT		PORTC

#define PIN_SJA_ALE		3
#define PIN_SJA_ALE_DDR		DDRC
#define PIN_SJA_ALE_PORT	PORTC

#define PIN_SJA_WR		4
#define PIN_SJA_WR_DDR		DDRC
#define PIN_SJA_WR_PORT		PORTC

#define PIN_SJA_RD		5
#define PIN_SJA_RD_DDR		DDRC
#define PIN_SJA_RD_PORT		PORTC

#define PIN_SJA_RST		6
#define PIN_SJA_RST_DDR		DDRC
#define PIN_SJA_RST_PORT	PORTC
#define PIN_SJA_RST_PIN		PINC

#define PIN_SJA_INT		2
#define PIN_SJA_INT_DDR		DDRB
#define PIN_SJA_INT_PORT	PORTB
#define PIN_SJA_INT_PIN		PINB

/* NPN Transistor + Relay to control RES_ON of SJA1000 module */

#define PIN_RES_ON		7
#define PIN_RES_ON_DDR		DDRC
#define PIN_RES_ON_PORT		PORTC
