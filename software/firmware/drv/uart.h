#pragma once

#include <stdint.h>

void uart_init(const uint8_t port, const uint32_t baud);
void uart_set_rxbuf(const uint8_t port, uint8_t *buf, const uint8_t len);
void uart_enable_rxc_int(const uint8_t port, const uint8_t enable);
void uart_putc(const uint8_t port, char c);
void uart_puts(const uint8_t port, const char *s);
uint8_t uart_rxc(const uint8_t port);
char uart_getc(const uint8_t port);
