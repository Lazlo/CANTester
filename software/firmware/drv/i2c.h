#pragma once

#include <stdint.h>

void i2c_init(const uint32_t f_scl);
void i2c_write(const uint8_t sla, const uint8_t *data, const uint16_t len);
