#pragma once

void timer2_init(void);
void timer2_sethandler(void (*fp)(void));
void timer2_enable(void);
