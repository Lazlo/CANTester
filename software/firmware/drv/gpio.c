#include "gpio.h"

void gpio_set_direction(const struct gpio_struct *p, const enum gpio_direction direction)
{
	uint8_t mask = (1 << p->offset);

	if (direction)	*p->dir_reg |= mask;
	else		*p->dir_reg &= ~mask;
}

void gpio_set_direction_vport(const struct gpio_struct *p_list, const enum gpio_direction direction)
{
	uint8_t i;

	for (i = 0; i < 8; i++)
		gpio_set_direction(&p_list[i], direction);
}

uint8_t gpio_read(const struct gpio_struct *p)
{
	uint8_t mask = (1 << p->offset);
	return *p->in_reg & mask ? 1 : 0;
}

uint8_t gpio_read_vport(const struct gpio_struct *p_list)
{
	uint8_t byte = 0;
	uint8_t i;

	for (i = 0; i < 8; i++)
		if (gpio_read(&p_list[i]))
			byte |= (1 << i);
	return byte;
}

void gpio_write(const struct gpio_struct *p, const uint8_t bit)
{
	uint8_t mask = (1 << p->offset);
	if (bit)	*p->out_reg |= mask;
	else		*p->out_reg &= ~mask;
}

void gpio_write_vport(const struct gpio_struct *p_list, const uint8_t byte)
{
	uint8_t i;

	for (i = 0; i < 8; i++)
		gpio_write(&p_list[i], byte & (1 << i) ? 1 : 0);
}
