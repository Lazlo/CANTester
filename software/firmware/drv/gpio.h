#pragma once

#include <stdint.h>

struct gpio_struct {
	uint8_t offset;
	volatile uint8_t *dir_reg;
	volatile uint8_t *in_reg;
	volatile uint8_t *out_reg;
};

enum gpio_direction {
	GPIO_INPUT = 0,
	GPIO_OUTPUT
};

void gpio_set_direction(const struct gpio_struct *p, const enum gpio_direction direction);
void gpio_set_direction_vport(const struct gpio_struct *p_list, const enum gpio_direction direction);
uint8_t gpio_read(const struct gpio_struct *p);
uint8_t gpio_read_vport(const struct gpio_struct *p_list);
void gpio_write(const struct gpio_struct *p, const uint8_t bit);
void gpio_write_vport(const struct gpio_struct *p_list, const uint8_t byte);
