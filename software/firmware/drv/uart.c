#include "uart.h"
#include "ring_buffer.h"
#include <avr/io.h>
#include <avr/interrupt.h>

#if defined(__AVR_ATmega1284P__)
#define DUAL_UART
#endif

#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega644__) || defined(__AVR_ATmega1284P__)

#define UART_BAUDRATE_REGH	UBRR0H
#define UART_BAUDRATE_REGL	UBRR0L
#define UART_CSA_REG		UCSR0A
#define UART_RXC_BIT		RXC0
#define UART_DRE_BIT		UDRE0
#define UART_CSB_REG		UCSR0B
#define UART_RXCIE_BIT		RXCIE0
#define UART_RXEN_BIT		RXEN0
#define UART_TXEN_BIT		TXEN0
#define UART_DATA_REG		UDR0

#ifdef DUAL_UART

#define UART1_BAUDRATE_REGH	UBRR1H
#define UART1_BAUDRATE_REGL	UBRR1L
#define UART1_CSA_REG		UCSR1A
#define UART1_CSB_REG		UCSR1B
#define UART1_DATA_REG		UDR1

#endif

#elif defined(__AVR_ATmega8__)

#define UART_BAUDRATE_REGH	UBRRH
#define UART_BAUDRATE_REGL	UBRRL
#define UART_CSA_REG		UCSRA
#define UART_DRE_BIT		UDRE
#define UART_CSB_REG		UCSRB
#define UART_RXCIE_BIT		RXCIE
#define UART_TXEN_BIT		TXEN
#define UART_DATA_REG		UDR

#else

#error No register mapping for selected target!

#endif

struct uart_struct {
	volatile uint8_t *baudrate_regh;
	volatile uint8_t *baudrate_regl;
	volatile uint8_t *csa_reg;
	volatile uint8_t *csb_reg;
	volatile uint8_t *csc_reg;
	volatile uint8_t *data_reg;

	struct ring_buffer_struct rxbuf;
};

static struct uart_struct uart[] = {
	{
		.baudrate_regh	= &UART_BAUDRATE_REGH,
		.baudrate_regl	= &UART_BAUDRATE_REGL,
		.csa_reg	= &UART_CSA_REG,
		.csb_reg	= &UART_CSB_REG,
		.csc_reg	= 0,
		.data_reg	= &UART_DATA_REG
	}
#ifdef DUAL_UART
	,
	{
		.baudrate_regh	= &UART1_BAUDRATE_REGH,
		.baudrate_regl	= &UART1_BAUDRATE_REGL,
		.csa_reg	= &UART1_CSA_REG,
		.csb_reg	= &UART1_CSB_REG,
		.csc_reg	= 0,
		.data_reg	= &UART1_DATA_REG
	}
#endif
};

static inline void uart_setbaudrate(const uint8_t port, const uint32_t baud)
{
	/* NOTE For some reason the correct formula results in a faulty UBRR
	 * value on the ATmega1284P with a 16 MHz crystal. */
	//uint16_t brr_val = (F_CPU / baud / 16) - 1;
	uint16_t brr_val = F_CPU / baud / 16;
	struct uart_struct *u;
	u = &uart[port];
	*u->baudrate_regh = brr_val >> 8;
	*u->baudrate_regl = brr_val & 0xFF;
}

static inline void uart_enable(const uint8_t port)
{
	struct uart_struct *u;
	u = &uart[port];
	*u->csb_reg |= (1 << UART_RXEN_BIT)|(1 << UART_TXEN_BIT);
}

void uart_init(const uint8_t port, const uint32_t baud)
{
	uart_setbaudrate(port, baud);
	uart_enable(port);

	ring_buffer_init(&uart[port].rxbuf);
}

void uart_set_rxbuf(const uint8_t port, uint8_t *buf, const uint8_t len)
{
	struct uart_struct *u;
	u = &uart[port];

	ring_buffer_set(&u->rxbuf, buf, len);
}

void uart_enable_rxc_int(const uint8_t port, const uint8_t enable)
{
	struct uart_struct *u;
	u = &uart[port];
	*u->csb_reg |= (1 << UART_RXCIE_BIT);
}

void uart_putc(const uint8_t port, char c)
{
	struct uart_struct *u;
	u = &uart[port];
	while (!(*u->csa_reg & (1 << UART_DRE_BIT)))
		;
	*u->data_reg = c;
}

void uart_puts(const uint8_t port, const char *s)
{
	while (*s)
		uart_putc(port, *s++);
}

uint8_t uart_rxc(const uint8_t port)
{
	struct uart_struct *u;
	u = &uart[port];
	return *u->csa_reg & (1 << UART_RXC_BIT) ? 1 : 0;
}

char uart_getc(const uint8_t port)
{
	struct uart_struct *u;
	u = &uart[port];

	/* When no ring buffer is used */
	if (u->rxbuf.len == 0)
		return *u->data_reg;
	return ring_buffer_get(&u->rxbuf);
}

ISR(USART1_RX_vect)
{
	struct uart_struct *u;
	u = &uart[1];

	ring_buffer_put(&u->rxbuf, *u->data_reg);
}
