#include "timer.h"
#include <avr/io.h>
#include <avr/interrupt.h>

#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega1284P__)

/* ATmega328P */
#define TIMER2_CA_REG		TCCR2A
#define TIMER2_CB_REG		TCCR2B
#define TIMER2_OC_REG		OCR2A
#define TIMER2_IMSK_REG		TIMSK2
#define TIMER2_OCIE_BIT		OCIE2A
#define TIMER2_COMP_INT_vect	TIMER2_COMPA_vect

#elif defined(__AVR_ATmega8__)

/* ATmega8 */
#define TIMER2_CA_REG		TCCR2
#define TIMER2_CB_REG		TCCR2
#define TIMER2_OC_REG		OCR2
#define TIMER2_IMSK_REG		TIMSK
#define TIMER2_OCIE_BIT		OCIE2
#define TIMER2_COMP_INT_vect	TIMER2_COMP_vect

#else

#error No register mapping for selected target!

#endif

static void (*timer2_compa_isr_fp)(void);

void timer2_init(void)
{
	/* Configure for 1ms period */
	/* F_OSC / 1000ms / ps 128 = 125 */

	TIMER2_CA_REG |= (1 << WGM21);			/* Set mode 2 (CTC) */
	TIMER2_OC_REG = 255 - 125;			/* reload value 130 = 255 - 125 */
	TIMER2_IMSK_REG |= (1 << TIMER2_OCIE_BIT);	/* Enable compare match A */
}

void timer2_sethandler(void (*fp)(void))
{
	timer2_compa_isr_fp = fp;
}

void timer2_enable(void)
{
	TIMER2_CB_REG |= (1 << CS22)|(1 << CS20);	/* clock source F_OSC / 128 */
}

ISR(TIMER2_COMP_INT_vect)
{
	(*timer2_compa_isr_fp)();
}
