#include "i2c.h"
#include "config_pins.h"

static void i2c_start(void)
{
	TWCR = (1 << TWINT)|(1 << TWSTA)|(1 << TWEN);
}

static void i2c_stop(void)
{
	TWCR = (1 << TWINT)|(1 << TWSTO)|(1 << TWEN);
}

static void i2c_busywait(void)
{
	while (!(TWCR & (1 << TWINT)))
		;
}

void i2c_init(const uint32_t f_scl)
{
	/* Enable internal pull-up resistors */
	PIN_I2C_SDA_PORT |= (1 << PIN_I2C_SDA);
	PIN_I2C_SCL_PORT |= (1 << PIN_I2C_SCL);
	/* Set bitrate */
	TWBR = ((F_OSC / f_scl) - 16) / 2;
	TWCR |= (1 << TWEN);
}

void i2c_write(const uint8_t sla, const uint8_t *data, const uint16_t len)
{
	uint16_t i;

	i2c_start();
	i2c_busywait();

	TWDR = (sla << 1);
	TWCR = (1 << TWINT)|(1 << TWEN);
	i2c_busywait();

	for (i = 0; i < len; i++) {
		TWDR = data[i];
		TWCR = (1 << TWINT)|(1 << TWEN);
		i2c_busywait();
	}

	i2c_stop();
}
