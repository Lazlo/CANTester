#pragma once

#include <stdint.h>
#include "can.h"
#include "gpio.h"

enum sja_pins {
	SJA_AD0_PIN = 0,
	SJA_AD1_PIN,
	SJA_AD2_PIN,
	SJA_AD3_PIN,
	SJA_AD4_PIN,
	SJA_AD5_PIN,
	SJA_AD6_PIN,
	SJA_AD7_PIN,
	SJA_CS_PIN,
	SJA_ALE_PIN,
	SJA_WR_PIN,
	SJA_RD_PIN,
	SJA_RST_PIN,
	SJA_INT_PIN,
	SJA_RES_ON,

	SJA_MAX_PINS
};

void sja_init(const struct gpio_struct *sja_pins);
void sja_int(void);
void sja_set_res(const uint8_t on);
uint8_t sja_error(void);
uint8_t sja_tx(const struct can_frame *frm);
uint8_t sja_rx(struct can_frame *frm);
