#include "sja.h"
#include "config_pins.h"
#include "can.h"
#include "string.h" /* for my_memcpy */

#include "uart.h"
#include "strutils.h"

#include <util/delay.h> /* for _delay_ms() in sja_reset() */

/* #define SJA_DEBUG_INIT */
#define SJA_DEBUG_INT
#define SJA_RXQUEUE_LEN 4

enum sja_registers {
	/* Control Segment */
	CR = 0,			/* Control Register */
	CMR,			/* Command Register */
	SR,			/* Status Register */
	IR,			/* Interrupt Register */
	ACR,			/* Acceptance Code Register */
	AMR,			/* Acceptance Mask Register */
	BTR0,			/* Bus timing 0 Register */
	BTR1,			/* Bus timing 1 Register */
	OC,			/* Output control Register */
	TEST,
	/* Transmit Buffer Segment */
	TX_IDENTIFIER_10TO3,
	TX_IDENTIFIER_2TO0,
	TX_BYTE1,
	TX_BYTE2,
	TX_BYTE3,
	TX_BYTE4,
	TX_BYTE5,
	TX_BYTE6,
	TX_BYTE7,
	TX_BYTE8,
	/* Receive Buffer Segment */
	RX_IDENTIFIER_10TO3,
	RX_IDENTIFIER_2TO0,
	RX_BYTE1,
	RX_BYTE2,
	RX_BYTE3,
	RX_BYTE4,
	RX_BYTE5,
	RX_BYTE6,
	RX_BYTE7,
	RX_BYTE8,
	/* Misc Segment */
	EMPTY,
	CDR			/* Clock Divider Register */
};

enum sja_control_bits {
	RR = 0,		/* Reset Request */
	RIE,		/* Receive Interrupt Enable */
	TIE,		/* Transmit Interrupt Enable */
	EIE,		/* Error Interrupt Enable */
	OIE		/* Overrun Interrupt Enable */
};

enum sja_command_bits {
	TR = 0,		/* Transmission Request */
	AT,		/* Abort Transmission */
	RRB,		/* Release Receive Buffer */
	CDO,		/* Clear Data Overrun */
	GTS		/* Go To Sleep */
};

enum sja_status_bits {
	RBS = 0,	/* Receive Buffer Status */
	DOS,		/* Data Overrun Status */
	TBS,		/* Transmit Buffer Status */
	TCS,		/* Transmission Complete Status */
	RS,		/* Receive Status */
	TS,		/* Transmit Status */
	ES,		/* Error Status */
	BS		/* Bus Status */
};

enum sja_interrupt_bits {
	RI = 0,		/* Receive Interrupt */
	TI,		/* Transmit Interrupt */
	EI,		/* Error Interrupt */
	DOI,		/* Data Overrun Interrupt */
	WUI		/* Wake-Up Interrupt */
};

enum sja_output_control_bits {
	OCMODE0 = 0,
	OCMODE1,
	OCPOL0,
	OCTN0,
	OCTP0,
	OCPOL1,
	OCTN1,
	OCTP1
};

enum sja_clock_divider_bits {
	CD0 = 0,
	CD1,
	CD2,
	CLOCK_OFF,
	ZERO,
	RXINTEN,
	CBP,
	CAN_MODE
};

struct sja_dev_struct {
	const struct gpio_struct *pins;
	struct can_frame rx_queue[SJA_RXQUEUE_LEN];
	uint8_t rx_queue_len;
	uint8_t rx_queue_head;
	uint8_t rx_queue_tail;
};

static struct sja_dev_struct sja_dev;

static void sja_rx_queue_init(struct sja_dev_struct *dev, const uint8_t rx_queue_len)
{
	my_memset((uint8_t *)&dev->rx_queue, 0, rx_queue_len * sizeof(struct can_frame));
	dev->rx_queue_len = rx_queue_len;
	dev->rx_queue_head = 0;
	dev->rx_queue_tail = 0;
}

static void sja_rx_queue_advance_head(struct sja_dev_struct *dev)
{
	if (dev->rx_queue_head == dev->rx_queue_len - 1)	dev->rx_queue_head = 0;
	else							dev->rx_queue_head++;
}

static void sja_rx_queue_advance_tail(struct sja_dev_struct *dev)
{
	if (dev->rx_queue_tail == dev->rx_queue_len - 1)	dev->rx_queue_tail = 0;
	else							dev->rx_queue_tail++;
}

static void sja_init_pins(const const struct gpio_struct *pins)
{
	const struct gpio_struct *p;
	uint8_t i;
	uint8_t direction;

	/* Set direction of all control pins to output
	 * except for the interrupt pin */
	for (i = SJA_CS_PIN; i < SJA_MAX_PINS; i++) {
		p = &pins[i];
		direction = (i != SJA_INT_PIN) ? GPIO_OUTPUT : GPIO_INPUT;
		gpio_set_direction(p, direction);
	}

	/* Set output of all pins to high
	 * except for ALE (and INT - which is an input anyways) and RES_ON pin. */
	for (i = SJA_CS_PIN; i < SJA_MAX_PINS; i++) {
		if (i == SJA_ALE_PIN || i == SJA_INT_PIN || i == SJA_RES_ON)
			continue;
		p = &pins[i];
		gpio_write(p, 1);
	}
}

static void sja_reset(const struct sja_dev_struct *dev)
{
	const struct gpio_struct *p = &dev->pins[SJA_RST_PIN];

	gpio_write(p, 0);
	_delay_ms(1);
	gpio_write(p, 1);
}

static void sja_select(const struct sja_dev_struct *dev, const uint8_t select)
{
	const struct gpio_struct *p = &dev->pins[SJA_CS_PIN];

	gpio_write(p, select ? 0 : 1);
}

static void sja_out(const struct sja_dev_struct *dev, const uint8_t byte)
{
	gpio_set_direction_vport(dev->pins, GPIO_OUTPUT);
	gpio_write_vport(dev->pins, byte);
}

static uint8_t sja_in(const struct sja_dev_struct *dev)
{
	gpio_set_direction_vport(dev->pins, GPIO_INPUT);
	return gpio_read_vport(dev->pins);
}

static void sja_setaddr(const struct sja_dev_struct *dev, const uint8_t addr)
{
	const struct gpio_struct *p = &dev->pins[SJA_ALE_PIN];
	gpio_write(p, 1);
	sja_out(dev, addr);
	gpio_write(p, 0);
}

static void sja_setdata(const struct sja_dev_struct *dev, const uint8_t data)
{
	const struct gpio_struct *p = &dev->pins[SJA_WR_PIN];
	gpio_write(p, 0);
	sja_out(dev, data);
	gpio_write(p, 1);
}

static uint8_t sja_getdata(const struct sja_dev_struct *dev)
{
	const struct gpio_struct *p = &dev->pins[SJA_RD_PIN];
	uint8_t byte;
	gpio_write(p, 0);
	byte = sja_in(dev);
	gpio_write(p, 1);
	return byte;
}

static void sja_write(const struct sja_dev_struct *dev, const uint8_t addr, const uint8_t data)
{
	sja_setaddr(dev, addr);
	sja_select(dev, 1);
	sja_setdata(dev, data);
	sja_select(dev, 0);
}

static uint8_t sja_read(const struct sja_dev_struct *dev, const uint8_t addr)
{
	uint8_t data;
	sja_setaddr(dev, addr);
	sja_select(dev, 1);
	data = sja_getdata(dev);
	sja_select(dev, 0);
	return data;
}

static void sja_debug_reg(const struct sja_dev_struct *dev, const uint8_t addr, const char *reg_name)
{
	uint8_t data;
	char data_str[3];

	data = sja_read(dev, addr);
	itoa(data, data_str, 16);

	uart_puts(0, "SJA ");
	uart_puts(0, reg_name);
	uart_puts(0, " 0x");
	uart_puts(0, data_str);
	uart_puts(0, "\r\n");
}

static uint8_t sja_get_can_mode(const struct sja_dev_struct *dev)
{
	return sja_read(dev, CDR) & (1 << CAN_MODE) ? 1 : 0;
}

static void sja_set_reset_mode(const struct sja_dev_struct *dev, const uint8_t on)
{
	uint8_t cr;
	const uint8_t mask = (1 << RR);

	cr = sja_read(dev, CR);
	if (on)		cr |= mask;
	else		cr &= ~mask;
	sja_write(dev, CR, cr);
}

static void sja_set_bitrate(const struct sja_dev_struct *dev)
{
	/* NOTE Calcuation for BTR0 and BTR1 register values for 250kBit/s has
	 * been taken from http://www.bittiming.can-wiki.info/ */

	sja_write(dev, BTR0, 0x01);
	sja_write(dev, BTR1, 0x1C);
}

static void sja_set_output(const struct sja_dev_struct *dev)
{
	/* Configure output driver(s) for TX (set normal mode) */

	sja_write(dev, OC, (1 << OCTP0)|(1 << OCTN0)|(1 << OCMODE1));
}

static void sja_enable_interrupts(const struct sja_dev_struct *dev)
{
	/* Enable all interrupts */
	sja_write(dev, CR, (1 << RIE)|(1 << TIE)|(1 << EIE)|(1 << OIE));
}

static uint8_t sja_status(const struct sja_dev_struct *dev)
{
	return sja_read(dev, SR);
}

static uint8_t sja_ir(const struct sja_dev_struct *dev)
{
	return sja_read(dev, IR) & 0x1F;
}

#if 0
static void sja_status_print(const uint8_t status)
{
	if (status & (1 << RBS))
		uart_puts(0, "RXFIFO full\r\n");
	if (status & (1 << DOS))
		uart_puts(0, "overrun\r\n");
	if (status & (1 << TBS))
		uart_puts(0, "TX buf ready\r\n");
	if (status & (1 << TCS))
		uart_puts(0, "TX complete\r\n");
	if (status & (1 << RS))
		uart_puts(0, "RX busy\r\n");
	if (status & (1 << TS))
		uart_puts(0, "TX busy\r\n");
	if (status & (1 << ES))
		uart_puts(0, "ERR exceeded\r\n");
	if (status & (1 << BS))
		uart_puts(0, "BUS OFF\r\n");
}
#endif

static void sja_set_id(const struct sja_dev_struct *dev, const uint16_t id)
{
	uint8_t tmp;
	const uint8_t id_mask = (1 << 7)|(1 << 6)|(1 << 5);

	tmp = sja_read(dev, TX_IDENTIFIER_2TO0);
	tmp &= ~id_mask;
	tmp |= (id << 5) & id_mask;
	sja_write(dev, TX_IDENTIFIER_10TO3, id >> 3);
	sja_write(dev, TX_IDENTIFIER_2TO0, tmp);
}

static void sja_get_id(const struct sja_dev_struct *dev, uint16_t *id)
{
	*id |= sja_read(dev, RX_IDENTIFIER_10TO3) << 3;
	*id |= sja_read(dev, RX_IDENTIFIER_2TO0) >> 5;
}

static void sja_set_len(const struct sja_dev_struct *dev, const uint8_t len)
{
	uint8_t tmp;
	const uint8_t len_mask = (1 << 3)|(1 << 2)|(1 << 1)|(1 << 0);

	tmp = sja_read(dev, TX_IDENTIFIER_2TO0);
	tmp &= ~len_mask;
	tmp |= len & len_mask;
	sja_write(dev, TX_IDENTIFIER_2TO0, tmp);
}

static void sja_get_len(const struct sja_dev_struct *dev, uint8_t *len)
{
	*len = sja_read(dev, RX_IDENTIFIER_2TO0) & 0x0F;
}

static void sja_set_data(const struct sja_dev_struct *dev, const uint8_t *data, const uint8_t len)
{
	uint8_t i;

	for (i = 0; i < len; i++)
		sja_write(dev, TX_BYTE1 + i, data[i]);
}

static void sja_get_data(const struct sja_dev_struct *dev, uint8_t *data, const uint8_t len)
{
	uint8_t i;

	for (i = 0; i < len; i++)
		data[i] = sja_read(dev, RX_BYTE1 + i);
}

static uint8_t sja_do_tx(const struct sja_dev_struct *dev, const struct can_frame *frm)
{
	uint8_t status;
	uint16_t tout = 0xFFFF;

	sja_set_id(dev, frm->id);
	sja_set_len(dev, frm->len);
	sja_set_data(dev, frm->data, frm->len);

	/* set transmit request */
	sja_write(dev, CMR, (1 << TR));

	/* Wait while "Transmit Status" bit is set in status register */
	while ((status = sja_read(dev, SR)) & (1 << TS) && tout--)
		;

	/* Only show status debug output when the status differs from the expected 0x0C */
	if (status != ((1 << TCS)|(1 << TBS)))
		sja_debug_reg(dev, SR, "SR");

	/* Check if we ended up in reset mode because of BUS-OFF condition */
	if (sja_read(dev, CR) & (1 << RR))
		uart_puts(0, "SJA RST MODE\r\n");

	/* FIXME wait for interrupt */
	/* FIXME check for errors */

	if (!tout)
		return 1;
	return 0;
}

static uint8_t sja_do_rx(const struct sja_dev_struct *dev, struct can_frame *frm)
{
	uint8_t status;
	uint8_t cmd;

	/* Check if there is a frame ready to be read from the RX FIFO */
	/* And clear the two TX related bits we do not need */
	status = sja_status(dev);
	status &= ~((1 << TCS)|(1 << TBS));
	if ((status & (1 << RBS)) == 0)
		return 0;

	/* Only show status if it is not only the receive buffer status bit */
	if (status != (1 << RBS))
		sja_debug_reg(dev, SR, "SR");

	sja_get_id(dev, &frm->id);
	sja_get_len(dev, &frm->len);
	sja_get_data(dev, frm->data, frm->len);

	/* Set Relase Receive Buffer bit in command */
	cmd = (1 << RRB);
	/* If there was a data overrun, clear it along with releasing the receive buffer */
	if (status & (1 << DOS))
		cmd |= (1 << CDO);
	sja_write(dev, CMR, cmd);

	return 0;
}

void sja_init(const struct gpio_struct *pins)
{
	struct sja_dev_struct *dev = &sja_dev;

	dev->pins = pins;

	sja_rx_queue_init(dev, SJA_RXQUEUE_LEN);

	sja_init_pins(dev->pins);

	sja_reset(dev);

#ifdef SJA_DEBUG_INIT
	/* Check Clock Divider register for which mode the chip is in (BasicCAN or PeliCAN) */
	sja_debug_reg(dev, CDR, "CDR");
#endif

	uart_puts(0, "SJA ");
	uart_puts(0, sja_get_can_mode(dev) ? "Peli" : "Basic");
	uart_puts(0, "CAN mode\r\n");

	/* Enter reset mode */
	sja_set_reset_mode(dev, 1);
#ifdef SJA_DEBUG_INIT
	sja_debug_reg(dev, CR, "CR (after enter RR)");
#endif

	/* Now that we are in reset mode we can adjust the following registers
	 * - acceptance code
	 * - acceptance mask
	 * - bus timing registers 0 and 1
	 * - output control
	 */

	/* Set acceptance filter to accept all */
	sja_write(dev, ACR, 0x00);
	sja_write(dev, AMR, 0xFF);

	sja_set_bitrate(dev);
#ifdef SJA_DEBUG_INIT
	sja_debug_reg(dev, BTR0, "BTR0");
	sja_debug_reg(dev, BTR1, "BTR1");
#endif
	sja_set_output(dev);
#ifdef SJA_DEBUG_INIT
	sja_debug_reg(dev, OC, "OC");
#endif

	/* Exit reset mode */
	sja_set_reset_mode(dev, 0);
#ifdef SJA_DEBUG_INIT
	sja_debug_reg(dev, CR, "CR (after exit RR)");
#endif

	/* NOTE Write access to the control register is possible in all modes */

	sja_enable_interrupts(dev);
#ifdef SJA_DEBUG_INIT
	sja_debug_reg(dev, CR, "CR (after IE)");
#endif
}

void sja_int(void)
{
	struct sja_dev_struct *dev = &sja_dev;
	uint8_t ir = sja_ir(dev);
	uint8_t i;
#ifdef SJA_DEBUG_INT
	char tmp_str[3];

	/* Only provide debug output when interrupt is not only the receive interrupt */
	if (ir != (1 << RI)) {
		itoa(ir, tmp_str, 16);
		uart_puts(0, "SJA INT 0x");
		uart_puts(0, tmp_str);
		uart_puts(0, "\r\n");
	}
#endif

	/* Check for overrun interrupt */
	if (ir & (1 << DOI)) {
		uart_puts(0, "SJA OVR\r\n");
	}

	if (ir & (1 << RI)) {
		i = 0;
		sja_do_rx(dev, &dev->rx_queue[i]);
	}
}

void sja_set_res(const uint8_t on)
{
	struct sja_dev_struct *dev = &sja_dev;
	struct gpio_struct *p = &dev->pins[SJA_RES_ON];

	gpio_write(p, on ? 1 : 0);
}

uint8_t sja_error(void)
{
	struct sja_dev_struct *dev = &sja_dev;
	uint8_t status = sja_status(dev);
	if (status & (1 << BS))
		return 3;		/* CAN_STATE_BUS_OFF */
	if (status & (1 << ES))
		return 1;		/* ??? */
	return 0;			/* CAN_STATE_ERROR_ACTIVE */
}

uint8_t sja_tx(const struct can_frame *frm)
{
	struct sja_dev_struct *dev = &sja_dev;
	return sja_do_tx(dev, frm);
}

uint8_t sja_rx(struct can_frame *frm)
{
	struct sja_dev_struct *dev = &sja_dev;

	/* FIXME check if there is a frame in the RX queue */

	/* Get the can frame from rx queue */
	my_memcpy((uint8_t *)frm, (uint8_t *)&dev->rx_queue[0], sizeof(struct can_frame));

	/* Release the rx queue slot */
	my_memset((uint8_t *)&dev->rx_queue[0], 0, sizeof(struct can_frame));

	return 0;
}
