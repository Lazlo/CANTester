#include "hi311x.h"
#include "config_pins.h"
#include "spi.h"
#include "uart.h"

enum hi311x_reg {
	CTRL0,		/* Control Register 0 */
	CTRL1,		/* Control Register 1 */
	BTR0,		/* Bit Timing Register 0 */
	BTR1,		/* Bit Timing Register 1 */
	TEC,		/* Transmit Error Counter Register */
	REC,		/* Receive Error Counter Register */
	MESSTAT,	/* Message Stateus Register */
	ERR,		/* Error Register */
	INTF,		/* Interrupt Flag Register */
	INTE,		/* Interrupt Enable Register */
	STATF,		/* Status Flag Register */
	STATFE,		/* Status Flag Enable Register */
	GPINE,		/* General Purpose Pin Enable Register */
	TIMERUB,	/* Free-Running Timer Upper Byte Register */
	TIMERLB		/* Free-Running Timer Lower Byte Register */
};

#define OP_NA		0x00
#define OP_WRITE	0
#define OP_READ		1

static const uint8_t hi311x_opcode[][2] = {
	{0x14,	0xD2},
	{0x16,	0xD4},
	{0x18,	0xD6},
	{0x1A,	0xD8},
	{0x26,	0xEC},
	{0x24,	0xEA},
	{OP_NA,	0xDA},
	{OP_NA,	0xDC},
	{OP_NA,	0xDE},
	{0x1C,	0xE4},
	{OP_NA,	0xE2},
	{0x1E,	0xE6},
	{0x22,	0xE8},
	{OP_NA,	0xFA},
	{OP_NA,	0xFA}
};

static void hi311x_select(const uint8_t select)
{
	if (select)
		PIN_SPI_SS_PORT &= ~(1 << PIN_SPI_SS);
	else
		PIN_SPI_SS_PORT |= (1 << PIN_SPI_SS);
}

static uint8_t hi311x_probe(void)
{
	const uint8_t ctrl0_state_reset = 0x80;
	uint8_t ctrl0_state;

	hi311x_select(1);
	spi_trx(hi311x_opcode[CTRL0][OP_READ]);
	ctrl0_state = spi_trx(0);
	hi311x_select(0);

	if (ctrl0_state == ctrl0_state_reset)
		return 1;
	return 0;
}

void hi311x_init(void)
{
	PIN_SPI_SS_DDR |= (1 << PIN_SPI_SS);
	PIN_SPI_SS_PORT |= (1 << PIN_SPI_SS);

	uart_puts("HI311X probe ");
	if (!hi311x_probe()) {
		uart_puts("failed!\r\n");
		return;
	}
	uart_puts("ok\r\n");

	/* FIXME Perform initialization */
}
