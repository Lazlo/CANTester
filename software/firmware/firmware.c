#include "config.h"
#include "config_pins.h"
#include "uart.h"
#include "reset.h"
#include "i2c.h"
#include "ssd1306.h"
#include "uptime.h"
#include "timer.h"
#include "sched.h"
#include <avr/interrupt.h>
#include <avr/io.h> /* for external interrupt bits */
#include "cantester.h"
#include "can.h"
#include "gpio.h"
#include "sja.h"
#include "slcan.h"
#include "console.h"
#include "strutils.h"
#include "string.h"
#include "version.h"

#define UART1_RXBUF_SIZE 128
static uint8_t uart1_rxbuf[UART1_RXBUF_SIZE];

/* SJA1000 GPIOs */

static const struct gpio_struct sja_pins[] = {
	[SJA_AD0_PIN]	= {.offset = PIN_SJA_AD0,	.dir_reg = &PIN_SJA_AD0_DDR,	.in_reg = &PIN_SJA_AD0_PIN,	.out_reg = &PIN_SJA_AD0_PORT},
	[SJA_AD1_PIN]	= {.offset = PIN_SJA_AD1,	.dir_reg = &PIN_SJA_AD1_DDR,	.in_reg = &PIN_SJA_AD1_PIN,	.out_reg = &PIN_SJA_AD1_PORT},
	[SJA_AD2_PIN]	= {.offset = PIN_SJA_AD2,	.dir_reg = &PIN_SJA_AD2_DDR,	.in_reg = &PIN_SJA_AD2_PIN,	.out_reg = &PIN_SJA_AD2_PORT},
	[SJA_AD3_PIN]	= {.offset = PIN_SJA_AD3,	.dir_reg = &PIN_SJA_AD3_DDR,	.in_reg = &PIN_SJA_AD3_PIN,	.out_reg = &PIN_SJA_AD3_PORT},
	[SJA_AD4_PIN]	= {.offset = PIN_SJA_AD4,	.dir_reg = &PIN_SJA_AD4_DDR,	.in_reg = &PIN_SJA_AD4_PIN,	.out_reg = &PIN_SJA_AD4_PORT},
	[SJA_AD5_PIN]	= {.offset = PIN_SJA_AD5,	.dir_reg = &PIN_SJA_AD5_DDR,	.in_reg = &PIN_SJA_AD5_PIN,	.out_reg = &PIN_SJA_AD5_PORT},
	[SJA_AD6_PIN]	= {.offset = PIN_SJA_AD6,	.dir_reg = &PIN_SJA_AD6_DDR,	.in_reg = &PIN_SJA_AD6_PIN,	.out_reg = &PIN_SJA_AD6_PORT},
	[SJA_AD7_PIN]	= {.offset = PIN_SJA_AD7,	.dir_reg = &PIN_SJA_AD7_DDR,	.in_reg = &PIN_SJA_AD7_PIN,	.out_reg = &PIN_SJA_AD7_PORT},
	[SJA_CS_PIN]	= {.offset = PIN_SJA_CS,	.dir_reg = &PIN_SJA_CS_DDR,	.in_reg = 0,			.out_reg = &PIN_SJA_CS_PORT},
	[SJA_ALE_PIN]	= {.offset = PIN_SJA_ALE,	.dir_reg = &PIN_SJA_ALE_DDR,	.in_reg = 0,			.out_reg = &PIN_SJA_ALE_PORT},
	[SJA_WR_PIN]	= {.offset = PIN_SJA_WR,	.dir_reg = &PIN_SJA_WR_DDR,	.in_reg = 0,			.out_reg = &PIN_SJA_WR_PORT},
	[SJA_RD_PIN]	= {.offset = PIN_SJA_RD,	.dir_reg = &PIN_SJA_RD_DDR,	.in_reg = 0,			.out_reg = &PIN_SJA_RD_PORT},
	[SJA_RST_PIN]	= {.offset = PIN_SJA_RST,	.dir_reg = &PIN_SJA_RST_DDR,	.in_reg = &PIN_SJA_RST_PIN,	.out_reg = &PIN_SJA_RST_PORT},
	[SJA_INT_PIN]	= {.offset = PIN_SJA_INT,	.dir_reg = &PIN_SJA_INT_DDR,	.in_reg = &PIN_SJA_INT_PIN,	.out_reg = &PIN_SJA_INT_PORT},
	[SJA_RES_ON]	= {.offset = PIN_RES_ON,	.dir_reg = &PIN_RES_ON_DDR,	.in_reg = 0,			.out_reg = &PIN_RES_ON_PORT},
};

static struct cantester_struct ct;

static char uptime_str[9];

static void uptime_show(void)
{
	uptime_get(uptime_str);
	console_puts(uptime_str);
	console_puts("\r\n");
}

static void uptime_task(void)
{
	uptime_tick();
#if 0
	uptime_show();
#endif
}

static void system_reset(void)
{
	((void(*)(void))0x0)();
}

static uint8_t ssd1306_initialized = 0;
static uint8_t blink;

struct cantester_status_struct {
	char mode_str[8];
	char state_str[10];
	char hw_str[11];
	char id_str[8];
	char tx_count_str[14];
	char rx_count_str[14];
	char can_state_str[19];
};

static struct cantester_status_struct ct_status = {
	.mode_str	= "Mode XX",
	.state_str	= "State XXX",
	.hw_str		= "HW XXXXXX",
	.id_str		= "ID XXXX",
	.tx_count_str	= "TX 0",
	.rx_count_str	= "RX 0",
	.can_state_str	= "can0 ",
};

static void cantester_status_get(const struct cantester_struct *ct,
				struct cantester_status_struct *st) {
	st->mode_str[5] = (ct->mode == CT_MODE_TX) ? 'T' : 'R';
	strcpy(&st->state_str[6], (ct->state == CT_STATE_ON) ? "on" : "off");

	if (ct->error) {
		strcpy(&st->can_state_str[5], blink ? "BUS-OFF" : "");
		blink = blink ? 0 : 1;
	} else {
		strcpy(&st->can_state_str[5], "ERROR-ACTIVE");
	}

	itoa(ct->dev.id, st->id_str+3, 10);
	switch (ct->hw) {
	case CT_HW_SLCAN:	strcpy(&st->hw_str[3], "SLCAN");	break;
	case CT_HW_SJA1000:	strcpy(&st->hw_str[3], "SJA1000");	break;
	case CT_HW_HI311X:	strcpy(&st->hw_str[3], "HI311X");	break;
	default:		strcpy(&st->hw_str[3], "none");		break;
	}
	itoa(ct->dev.stats.tx_packets, st->tx_count_str+3, 10);
	itoa(ct->dev.stats.rx_packets, st->rx_count_str+3, 10);
}

static void ssd1306_task(void)
{
	if (!ssd1306_initialized) {
		ssd1306_init(SSD1306_SLA);
		ssd1306_initialized = 1;
	}

	uptime_get(uptime_str);
	cantester_status_get(&ct, &ct_status);

	/* display lines (for 7x5 pixel font) */
	enum {
		LINE1 = 0,	/* yellow */
		LINE2 = 8,	/* yellow */
		LINE3 = 16,	/* blue */
		LINE4 = 24,	/* blue */
		LINE5 = 32,	/* blue */
		LINE6 = 40,	/* blue */
		LINE7 = 48,	/* blue */
		LINE8 = 56	/* blue */
	};

	ssd1306_clear();
	ssd1306_write(79, LINE1, uptime_str);
	ssd1306_write(0, LINE2, ct_status.mode_str);
	ssd1306_write(64, LINE2, ct_status.state_str);
	ssd1306_write(0, LINE3, ct_status.can_state_str);
	/* FIXME Display baudrate for SLCAN */
	ssd1306_write(0, LINE4, "Bitrate 250k");
	ssd1306_write(85, LINE4, ct_status.id_str);
	ssd1306_write(0, LINE5, ct_status.hw_str);
	ssd1306_write(0, LINE6, ct_status.rx_count_str);
	ssd1306_write(0, LINE7, ct_status.tx_count_str);
	ssd1306_display();
}

/* Wrappers to be passed to console module */

static void uart0_putc(char c)	{ uart_putc(0, c); }
static void uart0_puts(char *s)	{ uart_puts(0, s); }
static char uart0_getc(void)	{ return uart_getc(0); }
static uint8_t uart0_rxc(void)	{ return uart_rxc(0); }

/* Wrappers to be passed to SLCAN module */

static void uart1_puts(char *s)	{ uart_puts(1, s); }
static char uart1_getc(void)	{ return uart_getc(1); }
static uint8_t uart1_rxc(void)	{ return uart_rxc(1); }

static void can_print_frm(const uint16_t id, const uint8_t len, const uint8_t *data)
{
	uint8_t i;
	char id_str[4];
	char len_str[2];
	char data_str[17];

	itoa(id, id_str, 16);
	itoa(len, len_str, 10);
	for (i = 0; i < len; i++)
		itoa(data[i], &data_str[(2 * i)], 16);

	console_puts(id_str);
	console_puts(" ");
	console_puts(len_str);
	console_puts(" ");
	console_puts(data_str);
	console_puts("\r\n");
}

static void cantester_cmd_status(void)
{
	/* 1st line */
	console_puts(ct_status.state_str);
	console_puts( " ");
	console_puts(ct_status.mode_str);
	console_puts("\r\n");
	/* 2nd line */
	console_puts(ct_status.can_state_str);
	console_puts("\r\n");
	/* 3rd line */
	console_puts("Bitrate 250 ");
	console_puts(ct_status.id_str);
	console_puts("\r\n");
	/* 4th line */
	console_puts(ct_status.hw_str);
	console_puts("\r\n");
	/* 5th line */
	console_puts(ct_status.rx_count_str);
	console_puts("\r\n");
	/* 6th line */
	console_puts(ct_status.tx_count_str);
	console_puts("\r\n");
}

static void can_task(void)
{
	uint8_t i;
	struct can_frame frm;

	if (ct.state == CT_STATE_OFF)
		return;

	switch (ct.mode) {
	case CT_MODE_RX:
		frm.len = 0;
		cantester_recv(&ct, &frm);
		if (frm.len == 0)
			return;
		console_puts("RX ");
		can_print_frm(frm.id, frm.len, frm.data);
		break;
	case CT_MODE_TX:
		/* Convert 64 bit TX counter to array of 8 bytes */
		frm.id = ct.dev.id;
		frm.len = 8;
		for (i = 0; i < frm.len; i++)
			frm.data[i] = (ct.dev.stats.tx_packets >> (frm.len * i));

		cantester_send(&ct, &frm);
		break;
	}
}

int main(void)
{
	uart_init(0, UART_BAUD);
	uart_puts(0, "CANTester " VERSION "\r\n");
	uart_puts(0, "UART0 ready!\r\n");
	reset_cause_print(uart0_puts);
	uptime_init();
	i2c_init(I2C_F_SCL);
	sja_init((const struct gpio_struct *)&sja_pins);
	/* Enabel Pull-Up on pin connected to SJA1000 interrupt pin */
	PIN_SJA_INT_PORT |= (1 << PIN_SJA_INT);
	/* Configure and enable external interrupt pin connected to SJA1000 (INT2) */
	EICRA &= ~((1 << ISC21)|(1 << ISC20)); /* clear mask - trigger on low level */
	EIMSK |= (1 << INT2);
	uart_init(1, UART_BAUD);
	uart_set_rxbuf(1, uart1_rxbuf, UART1_RXBUF_SIZE);
	uart_enable_rxc_int(1, 1);
	slcan_init(uart1_puts, uart1_rxc, uart1_getc);
	cantester_init(&ct);
	console_init(uart0_putc, uart0_getc, uart0_rxc);
	console_addcmd("reset",		system_reset);
	console_addcmd("uptime",	uptime_show);
	console_addcmd("ct-reset",	cantester_cmd_reset);
	console_addcmd("ct-on",		cantester_cmd_on);
	console_addcmd("ct-off",	cantester_cmd_off);
	console_addcmd("ct-tx",		cantester_cmd_tx);
	console_addcmd("ct-rx",		cantester_cmd_rx);
	console_addcmd("ct-hw-slcan",	cantester_cmd_hw_slcan);
	console_addcmd("ct-hw-sja",	cantester_cmd_hw_sja);
	console_addcmd("ct-res-on",	cantester_cmd_res_on);
	console_addcmd("ct-res-off",	cantester_cmd_res_off);
	console_addcmd("ct-status",	cantester_cmd_status);
	sched_init();
	sched_addtask(console_update, 5, 0);
	sched_addtask(uptime_task, 1000, 0);
	sched_addtask(ssd1306_task, 250, 0);
	sched_addtask(can_task, 1, 0);
	timer2_init();
	timer2_sethandler(sched_update);
	timer2_enable();
	sei();

	while (1) {
		sched_dispatch();
	}
	return 0;
}

ISR(INT2_vect) {
	sja_int();
}
