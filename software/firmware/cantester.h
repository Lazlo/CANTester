#pragma once

#include "can.h" /* for can_dev_struct */
#include <stdint.h>

enum cantester_state {
	CT_STATE_OFF = 0,
	CT_STATE_ON
};

enum cantester_mode {
	CT_MODE_RX = 0,
	CT_MODE_TX
};

enum cantester_hw {
	CT_HW_SLCAN = 0,
	CT_HW_SJA1000,
	CT_HW_HI311X
};

struct cantester_struct {
	enum cantester_state state;
	enum cantester_mode mode;
	enum cantester_hw hw;
	struct can_dev_struct dev;
	uint8_t error;
};

void cantester_init(struct cantester_struct *ct);
void cantester_cmd_reset(void);
void cantester_cmd_on(void);
void cantester_cmd_off(void);
void cantester_cmd_tx(void);
void cantester_cmd_rx(void);
void cantester_cmd_hw_slcan(void);
void cantester_cmd_hw_sja(void);
void cantester_cmd_res_on(void);
void cantester_cmd_res_off(void);
uint8_t cantester_send(struct cantester_struct *ct, const struct can_frame *frm);
uint8_t cantester_recv(struct cantester_struct *ct, struct can_frame *frm);
