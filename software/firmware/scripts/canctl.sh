#!/bin/bash

set -e
set -u

main() {
	if [ $# != 1 ]; then
		echo "USAGE: $0 <action>"
		echo ""
		echo "ACTIONS"
		echo ""
		echo -e "\tup"
		echo -e "\tdown"
		echo -e "\twatch"
		echo ""
		exit 1
	fi

	action="$1"

	ifname="can0"
	bitrate="250000"

	case $action in
	"up"|"down")
		if [ $UID != 0 ]; then
			echo "ERROR: Run as root."
			exit 1
		fi
		if [ "$action" == "up" ]; then
			ip link set dev $ifname type can bitrate $bitrate restart-ms 1
		fi
		ip link set $action $ifname
		;;
	"watch")
		watch -n 1 -d ip -details -statistics -statistics link show dev $ifname
		;;
	esac
}

main $@
