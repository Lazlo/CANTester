#!/bin/bash

set -e
set -u

echo -e "#pragma once\n\n#define VERSION \"$(git describe --tags --dirty --long)\"" > version.h
