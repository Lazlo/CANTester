#pragma once

#define UART_BAUD		115200

#define I2C_F_SCL		400000

#define SSD1306_SLA		0x3C

#define SSD1306_LCDHEIGHT	64
#define SSD1306_LCDWIDTH	128

#define SCHED_TASKS_MAX		4
