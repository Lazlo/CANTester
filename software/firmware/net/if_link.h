#pragma once

#include <stdint.h>

struct if_link_stats_struct {
	uint64_t rx_packets;
	uint64_t rx_over;
	uint64_t tx_packets;
};

inline void if_link_stats_reset(struct if_link_stats_struct *stats)
{
	stats->rx_packets = 0;
	stats->rx_over = 0;
	stats->tx_packets = 0;
}
