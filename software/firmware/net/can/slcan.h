#pragma once

#include "can.h"

void slcan_init(void (*fp_tx_puts)(char *), uint8_t (*fp_rxc)(void), char (*fp_rx_getc)(void));
void slcan_send(const struct can_frame *frm);
uint8_t slcan_recv(struct can_frame *frm);
