#pragma once

enum can_state {
	CAN_STATE_ERROR_ACTIVE = 0;	/* RX/TX error count < 96 */
	CAN_STATE_ERROR_WARNING,	/* RX/TX error count < 128 */
	CAN_STATE_ERROR_PASSIVE,	/* RX/TX error count < 256 */
	CAN_STATE_BUS_OFF		/* RX/TX error count >= 256 */
};
