#include "slcan.h"
#include "string.h"

static void (*fp_tx_puts)(char *);
static uint8_t (*fp_rx_complete)(void);
static char (*fp_rx_getc)(void);
static char slcan_rx_line[24];
static uint8_t slcan_rx_line_idx;

static void slcan_puts(char *s)
{
	(*fp_tx_puts)(s);
}

static uint8_t slcan_rxc(void)
{
	return (*fp_rx_complete)();
}

static char slcan_getc(void)
{
	return (*fp_rx_getc)();
}

static void int2hex(uint8_t n, char *s)
{
	uint8_t h = n / 16;
	uint8_t l = n % 16;

	if (h > 9)
		s[0] = 'A' + (h - 10);
	else
		s[0] = '0' + h;
	if (l > 9)
		s[1] = 'A' + (l - 10);
	else
		s[1] = '0' + l;
}

void slcan_init(void (*fp_puts)(char *), uint8_t (*fp_rxc)(void), char (*fp_getc)(void))
{
	fp_tx_puts = fp_puts;
	fp_rx_complete = fp_rxc;
	fp_rx_getc = fp_getc;

	/* Initialize line buffer and index */
	my_memset((uint8_t *)slcan_rx_line, 0, 24);
	slcan_rx_line_idx = 0;
}

void slcan_send(const struct can_frame *frm)
{
	/* char slcan_frm[] = "tiiildddddddddddddddd\r"; */
	char id_str[4];
	char len_str[2];
	char data_str[17];
	uint8_t i;

	/* convert ID to ascii string */

	id_str[0] = '0' + frm->id / 100;
	id_str[1] = '0' + (frm->id % 100) / 10;
	id_str[2] = '0' + frm->id % 10;
	id_str[3] = '\0';

	/* convert length to ascii string */

	len_str[0] = '0' + frm->len;
	len_str[1] = '\0';

	/* convert data to ascii HEX string */

	for (i = 0; i < frm->len; i++)
		int2hex(frm->data[i], &data_str[2 * i]);
	data_str[2 * frm->len] = '\0';

	slcan_puts("t");
	slcan_puts(id_str);
	slcan_puts(len_str);
	slcan_puts(data_str);
	slcan_puts("\r");
}

uint8_t slcan_recv(struct can_frame *frm)
{
	return 0;
}
