#pragma once

#include "if_link.h"

struct can_frame {
	uint16_t id;
	uint8_t len;
	uint8_t data[8];
};

struct can_dev_struct {
	uint16_t id;
	struct if_link_stats_struct stats;
};

void can_init(void);
