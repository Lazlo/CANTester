#include "console.h"
#include "string.h"

#define CONSOLE_MAX_CMD_LEN 64
#define CONSOLE_RX_LINE_LEN 80
#define CONSOLE_MAX_CMDS 16
#define CONSOLE_PROMPT_STR ">>"

struct console_cmd_struct {
	char cmd[CONSOLE_MAX_CMD_LEN];
	void (*fp)(void);
};

struct console_struct {
	void (*fp_putc)(char);
	char (*fp_getc)(void);
	uint8_t (*fp_rxc)(void);
	uint8_t rx_line_i;
	char rx_line[CONSOLE_RX_LINE_LEN];
	struct console_cmd_struct command[CONSOLE_MAX_CMDS];
};

static struct console_struct con;

static void console_prompt(void)
{
	console_puts(CONSOLE_PROMPT_STR " ");
}

static void console_help(void)
{
	uint8_t i;

	for (i = 0; i < CONSOLE_MAX_CMDS; i++) {
		if (con.command[i].fp == 0)
			continue;
		console_puts(con.command[i].cmd);
		console_puts("\r\n");
	}
	console_puts("help\r\n");
}

static void console_dispatch(char *s)
{
	uint8_t i;

	for (i = 0; i < CONSOLE_MAX_CMDS; i++) {
		if (con.command[i].fp == 0)
			continue;
		if (my_strcmp(con.command[i].cmd, s) != 0)
			continue;
		(con.command[i].fp)();
		return;
	}

	/* Try the default commands if none of the ones from
	 * the command list resulted in a match */

	if (my_strcmp(s, "help") == 0) {
		console_help();
	} else {
		console_puts("ERROR unknown command\r\n");
	}
}

void console_init(void (*fp_putc)(char),
			char (*fp_getc)(void),
			uint8_t (*fp_rxc)(void))
{
	uint8_t i;

	con.fp_putc = fp_putc;
	con.fp_getc = fp_getc;
	con.fp_rxc = fp_rxc;

	/* clear list of commands */
	for (i = 0; i < CONSOLE_MAX_CMDS; i++)
		my_memset((uint8_t *)&con.command[i], 0, sizeof(struct console_cmd_struct));

	console_prompt();
}

void console_addcmd(const char *cmd, void (*fp)(void))
{
	uint8_t i;

	/* Find free slot in command list */
	for (i = 0; i < CONSOLE_MAX_CMDS; i++)
		if (con.command[i].fp == 0)
			break;

	/* No free slot found */
	if (i == CONSOLE_MAX_CMDS)
		return;

	strcpy((char *)&con.command[i].cmd, cmd);
	con.command[i].fp = fp;
}

void console_puts(char *s)
{
	while (*s)
		(*con.fp_putc)(*s++);
}

void console_update(void)
{
	char c;

	if (!(*con.fp_rxc)())
		return;
	c = (*con.fp_getc)();
	/* Collect each character of a line until \r */
	if (c != '\r')
	{
		con.rx_line[con.rx_line_i++] = c;
		(*con.fp_putc)(c);
		return;
	}
	if (con.rx_line_i > 0) {
		con.rx_line[con.rx_line_i] = '\0';
		con.rx_line_i = 0;
		console_puts("\r\n");
#if 0
		console_puts("READ ");
		console_puts(con.rx_line);
		console_puts("\r\n");
#endif
		console_dispatch(con.rx_line);
	} else {
		console_puts("\r\n");
	}
	console_prompt();
}
