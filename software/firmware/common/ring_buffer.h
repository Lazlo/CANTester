#pragma once

#include <stdint.h>

struct ring_buffer_struct {
	uint8_t *buf;
	uint8_t len;
	uint8_t head;
	uint8_t tail;
};

void ring_buffer_init(struct ring_buffer_struct *rb);
void ring_buffer_set(struct ring_buffer_struct *rb, uint8_t *buf, const uint8_t len);
void ring_buffer_put(struct ring_buffer_struct *rb, const uint8_t byte);
uint8_t ring_buffer_get(struct ring_buffer_struct *rb);
