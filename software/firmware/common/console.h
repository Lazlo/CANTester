#pragma once

#include <stdint.h>

void console_init(void (*fp_putc)(char),
			char (*fp_getc)(void),
			uint8_t (*fp_rxc)(void));
void console_addcmd(const char *cmd, void (*fp)(void));
void console_puts(char *s);
void console_update(void);
