#pragma once

#include <stdint.h>

void my_memcpy(uint8_t *dest, const uint8_t *src, uint16_t len);
void my_memset(uint8_t *s, uint8_t c, const uint16_t n);
int8_t my_strcmp(char *s1, char *s2);
char *strcpy(char *dest, const char *src);
