#pragma once

#include <stdint.h>

void sched_init(void);
uint8_t sched_addtask(void (*fp)(void), const uint16_t period, const uint16_t delay);
void sched_update(void);
void sched_dispatch(void);
