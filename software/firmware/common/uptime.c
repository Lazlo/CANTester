#include "uptime.h"
#include <stdint.h>

struct uptime_struct {
	uint8_t h;
	uint8_t m;
	uint8_t s;
};

static struct uptime_struct uptime;

void uptime_init(void)
{
	uptime.h = 0;
	uptime.m = 0;
	uptime.s = 0;
}

void uptime_tick(void)
{
	if (++uptime.s < 60)
		return;
	uptime.s = 0;
	if (++uptime.m < 60)
		return;
	uptime.m = 0;
	if (++uptime.h < 24)
		return;
	uptime.h = 0;
}

void uptime_get(char *s)
{
	s[0] = '0' + uptime.h / 10;
	s[1] = '0' + uptime.h % 10;
	s[2] = ':';
	s[3] = '0' + uptime.m / 10;
	s[4] = '0' + uptime.m % 10;
	s[5] = ':';
	s[6] = '0' + uptime.s / 10;
	s[7] = '0' + uptime.s % 10;
	s[8] = '\0';
}
