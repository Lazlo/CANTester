#include "ring_buffer.h"

void ring_buffer_init(struct ring_buffer_struct *rb)
{
	rb->len = 0;
	rb->head = 0;
	rb->tail = 0;
}

void ring_buffer_set(struct ring_buffer_struct *rb, uint8_t *buf, const uint8_t len)
{
	rb->buf = buf;
	rb->len = len;
}

void ring_buffer_put(struct ring_buffer_struct *rb, const uint8_t byte)
{
	uint8_t *buf = rb->buf;
	uint8_t len = rb->len;
	uint8_t *head = &rb->head;
	uint8_t *tail = &rb->tail;
	uint8_t used = (*tail <= *head) ? *head - *tail : *head + (len - *tail);

	if (used == len)
		return;
	buf[*head] = byte;
	*head = *head < len - 1 ? *head + 1 : 0;
}

uint8_t ring_buffer_get(struct ring_buffer_struct *rb)
{
	uint8_t byte;

	byte = rb->buf[rb->tail];
	rb->tail = (rb->tail < rb->len - 1) ? rb->tail + 1 : 0;
	return byte;
}
