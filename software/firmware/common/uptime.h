#pragma once

void uptime_init(void);
void uptime_tick(void);
void uptime_get(char *s);
