/**
 * \file strutils.h
 * \brief string manipulation functions - interface definition
 */

#ifndef D_STRUTILS_H
#define D_STRUTILS_H

#include <stdint.h>

void strreverse( char *begin, char *end );

void itoa( int64_t value, char *str, int base );

#endif /* D_STRUTILS_H */
