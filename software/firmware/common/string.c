#include "string.h"

void my_memcpy(uint8_t *dest, const uint8_t *src, uint16_t len)
{
	uint16_t i;
	for (i = 0; i < len; i++)
		dest[i] = src[i];
}

void my_memset(uint8_t *s, uint8_t c, const uint16_t n)
{
	uint16_t i;
	for (i = 0; i < n; i++)
		s[i] = c;
}

int8_t my_strcmp(char *s1, char *s2)
{
	uint8_t i;
	for (i = 0; s1[i] != '\0' && s2[i] != '\0'; i++) {
		if (s1[i] != s2[i]) {
			return 1;
		}
	}
	return 0;
}

char *strcpy(char *dest, const char *src)
{
	uint16_t i = 0;
	while (src[i] != '\0') {
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return dest;
}
