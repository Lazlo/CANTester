#include "cantester.h"
#include "slcan.h"
#include "sja.h"
#include "if_link.h"

#include "config_pins.h"

static struct cantester_struct *s_ct;

void cantester_init(struct cantester_struct *ct)
{
	/* save pointer to CT struct */
	s_ct = ct;

	ct->state = CT_STATE_ON;
	ct->mode = CT_MODE_RX;
	ct->hw = CT_HW_SJA1000;
	ct->dev.id = 0x07FF;
	if_link_stats_reset(&ct->dev.stats);
	ct->error = 0;
}

void cantester_cmd_reset(void)
{
	if_link_stats_reset(&s_ct->dev.stats);
	s_ct->error = 0;
}

void cantester_cmd_on(void)
{
	s_ct->state = CT_STATE_ON;
}

void cantester_cmd_off(void)
{
	s_ct->state = CT_STATE_OFF;
}

void cantester_cmd_tx(void)
{
	s_ct->mode = CT_MODE_TX;
}

void cantester_cmd_rx(void)
{
	s_ct->mode = CT_MODE_RX;
}

void cantester_cmd_hw_slcan(void)
{
	s_ct->hw = CT_HW_SLCAN;
}

void cantester_cmd_hw_sja(void)
{
	s_ct->hw = CT_HW_SJA1000;
}

void cantester_cmd_res_on(void)
{
	sja_set_res(1);
}

void cantester_cmd_res_off(void)
{
	sja_set_res(0);
}

uint8_t cantester_send(struct cantester_struct *ct, const struct can_frame *frm)
{
	uint8_t rc = 0;

	switch (ct->hw) {
	case CT_HW_SLCAN:
		slcan_send(frm);
		ct->dev.stats.tx_packets++;
		break;
	case CT_HW_SJA1000:
		sja_tx(frm);
		ct->error = sja_error();
		if (!ct->error)
			ct->dev.stats.tx_packets++;
		break;
	case CT_HW_HI311X:
		break;
	}
	return rc;
}

uint8_t cantester_recv(struct cantester_struct *ct, struct can_frame *frm)
{
	uint8_t rc = 0;

	switch (ct->hw) {
	case CT_HW_SLCAN:
		slcan_recv(frm);
		if (frm->len > 0)
			ct->dev.stats.rx_packets++;
		break;
	case CT_HW_SJA1000:
		/* FIXME check return value of sja_rx() */
		sja_rx(frm);
		if (frm->len > 0)
			ct->dev.stats.rx_packets++;
		break;
	case CT_HW_HI311X:
		break;
	}
	return rc;
}
