extern "C"
{
#include "RingBuffer.h"
}

#include "CppUTest/TestHarness.h"

TEST_GROUP(RingBuffer)
{
	static const unsigned int rb_len = 4;
	struct ring_buffer rb;

	static void polute_rb(struct ring_buffer *rb)
	{
		rb->len = 5;
		rb->head = 23;
		rb->tail = 42;
	}

    void setup()
    {
	polute_rb(&rb);
      RingBuffer_Create(&rb, rb_len);
    }

    void teardown()
    {
       RingBuffer_Destroy();
    }
};

TEST(RingBuffer, Create_headIsZero)
{
	CHECK_EQUAL(0, rb.head);
}

TEST(RingBuffer, Create_tailIsZero)
{
	CHECK_EQUAL(0, rb.tail);
}

TEST(RingBuffer, Create_lenIsSetFromArgument)
{
	CHECK_EQUAL(rb_len, rb.len);
}

TEST(RingBuffer, Push_onSuccess_incrementsHeadByOne)
{
	RingBuffer_Push(&rb, (char)0xFF);
	CHECK_EQUAL(1, rb.head);
}

TEST(RingBuffer, Push_onFailure_doesNotChangeHead)
{
	unsigned int i;
	for (i = 0; i < rb_len; i++)
		RingBuffer_Push(&rb, (char)0xFF);
	RingBuffer_Push(&rb, (char)0xFF);
	CHECK_EQUAL(rb_len - 1, rb.head);

}

TEST(RingBuffer, Pop_onSuccess_incrementsTailByOne)
{
	char byte;
	RingBuffer_Push(&rb, (char)0xFF);
	RingBuffer_Pop(&rb, &byte);
	CHECK_EQUAL(1, rb.tail);
}

TEST(RingBuffer, Pop_onFailure_doesNotChangeTail)
{
	char byte;
	RingBuffer_Pop(&rb, &byte);
	CHECK_EQUAL(0, rb.tail);
}

TEST(RingBuffer, Push_onSuccess_returnsZero)
{
	unsigned int rc = RingBuffer_Push(&rb, (char)0x55);
	CHECK_EQUAL(0, rc);
}

TEST(RingBuffer, Push_onFailure_returnsOne)
{
	unsigned int i;
	for (i = 0; i < rb_len; i++)
		RingBuffer_Push(&rb, (char)0xFF);
	CHECK_EQUAL(1, RingBuffer_Push(&rb, (char)0xFF));
}

TEST(RingBuffer, Pop_onSuccess_returnsZero)
{
	unsigned int rc;
	char byte;
	RingBuffer_Push(&rb, (char)0xFF);
	rc = RingBuffer_Pop(&rb, &byte);
	CHECK_EQUAL(0, rc);
}

TEST(RingBuffer, Pop_onFailure_returnOne)
{
	unsigned int rc;
	char byte;
	rc = RingBuffer_Pop(&rb, &byte);
	CHECK_EQUAL(1, rc);
}

TEST(RingBuffer, GetFree_returnsLen_whenHeadEqualsTail)
{
	CHECK_EQUAL(rb_len, RingBuffer_GetFree(&rb));
}

TEST(RingBuffer, GetFree_returnsFree)
{
	RingBuffer_Push(&rb, (char)0xFF);
	RingBuffer_Push(&rb, (char)0xFF);
	CHECK_EQUAL(2, RingBuffer_GetFree(&rb));
}
