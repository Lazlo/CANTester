#ifndef D_libBuildTime_H
#define D_libBuildTime_H

///////////////////////////////////////////////////////////////////////////////
//
//  libBuildTime is responsible for recording and reporting when
//  this project library was built
//
///////////////////////////////////////////////////////////////////////////////

class libBuildTime
  {
  public:
    explicit libBuildTime();
    virtual ~libBuildTime();
    
    const char* GetDateTime();

  private:
      
    const char* dateTime;

    libBuildTime(const libBuildTime&);
    libBuildTime& operator=(const libBuildTime&);

  };

#endif  // D_libBuildTime_H
