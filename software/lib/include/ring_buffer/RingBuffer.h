#ifndef D_RingBuffer_H
#define D_RingBuffer_H

/**********************************************************
 *
 * RingBuffer is responsible for ...
 *
 **********************************************************/

struct ring_buffer {
	unsigned int len;
	unsigned int head;
	unsigned int tail;
};

void RingBuffer_Create(struct ring_buffer *rb, const unsigned int len);
void RingBuffer_Destroy(void);
unsigned int RingBuffer_Push(struct ring_buffer *rb, const char byte);
unsigned int RingBuffer_Pop(struct ring_buffer *rb, char *byte);
unsigned int RingBuffer_GetFree(const struct ring_buffer *rb);

#endif  /* D_FakeRingBuffer_H */
