#include "RingBuffer.h"

void RingBuffer_Create(struct ring_buffer *rb, const unsigned int len)
{
	rb->len = len;
	rb->head = 0;
	rb->tail = 0;
}

void RingBuffer_Destroy(void)
{
}

unsigned int RingBuffer_Push(struct ring_buffer *rb, const char byte)
{
	if (rb->head + 1 == rb->len)
		return 1;
	rb->head++;
	return 0;
}

unsigned int RingBuffer_Pop(struct ring_buffer *rb, char *byte)
{
	if (rb->head == 0)
		return 1;
	rb->tail++;
	return 0;
}

unsigned int RingBuffer_GetFree(const struct ring_buffer *rb)
{
	return rb->len - rb->head;
}
