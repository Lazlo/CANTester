#include "libBuildTime.h"

libBuildTime::libBuildTime()
: dateTime(__DATE__ " " __TIME__)
{
}

libBuildTime::~libBuildTime()
{
}

const char* libBuildTime::GetDateTime()
{
    return dateTime;
}

