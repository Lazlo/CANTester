# CANTester

A single channel CAN tester.

## Features

 * one CAN channel
   * NXP SJA1000 CAN controller
   * trun on/off 120 ohms resistor
   * SLCAN (RS232 @ 115200 bps)
 * BNC outputs for connecting CAN signals to an oscilloscope
 * OLED display with 128 by 64 pixels
 * Modes of Operation
   * single channel reception test
   * single channel transmission
 * Control Interfaces
   * 3-pin UART @ 115200 bps (+5V TTL)
 * supply voltage inputs
   * barrel socket (+12V)
   * ISP-6 (+5V)

## Hardware

The device is based on a AVR ATmega1284p 8-bit microcontroller running at 16MHz operating at +5 volts.

The primary UART of the microcontroller is used for control commands and is operated at 115200 bps.

The CAN controller used is a NXP SJA1000 CAN controller that is connected via a parallel bus interface
to the microcontroller. By adding a relay and a NPN transistor, the device is able to control the use
of the built-in 120 ohms CAN termination resistor.

Aside from the CAN controller, the ATmega contains two UARTs of which one is used to allow transmission
and reception of SLCAN frames (ASCII CAN via UART). The second UART is connected to a MAX232 level shifter
in order to allow interfacing with a RS232 bus.

### Block Diagram

```
           +-------------------------------------------------------+
    DB9 O--|--- RS232 SLCAN-----------------+                      |
           |                                |                      |
           |                  +-----------------------+            |
    DB9 O--|-+  +---------+   |                       |----console-|--O
           | +--| SJA1000 |---| Application Processor |            |
2 x BNC O--|-+  +---------+   |                       |-------ISP--|--O
           |                  +-----------------------+            |
           |                    |           |                      |
        O--|---Power In---------+           |                      |
           +-------------------------------------------------------+
                                            |
                                     +-------------+
                                     |   Display   |
                                     +-------------+
```

### Schematic & Circuit Board Layout

The schematic and circuit board layout have been created with KiCAD and can be found in the ```hardware/circuit/v1```
directory.

### Datasheets

Datasheets for all components are keept in the ```hardware/datasheets``` directory.

## Firmware

The firmware for the ATmega was written in C and can be found in the ```software/firmware``` directory.


[![Build Status](https://travis-ci.org/lazlo/CANTester.svg?branch=master)](https://travis-ci.org/lazlo/CANTester)

### Control Commands

List of commands available via the control interface.

| Command       | Description |
| ------------- | --------------------------------------------------------- |
| reset         | reboot the device                                         |
| uptime        | show time since last boot                                 |
| ct-reset      | restore CANTester default settings                        |
| ct-on         | start transmission/reception                              |
| ct-off        | stop transmission/reception                               |
| ct-tx         | transmit number sequence mode                             |
| ct-rx         | receive number sequence mode                              |
| ct-hw-slcan   | set CAN harware backend to SLCAN via RS232                |
| ct-hw-sja     | set CAN harware backend to SJA1000 CAN controller         |
| ct-res-on     | turn 120 ohm CAN termination resistor on (SJA mode only)  |
| ct-res-off    | turn 120 ohm CAN termination resistor off (SJA mode only) |
| ct-status     | display CANTester status and current settings             |
| help          | list all commands available                               |
