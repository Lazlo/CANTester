EESchema Schematic File Version 4
LIBS:CANTester-v2-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 3350 3150 1200 700 
U 5B8E4D4E
F0 "CANTester-v2-01-power" 50
F1 "CANTester-v2-01-power.sch" 50
$EndSheet
$Sheet
S 4700 3150 1200 700 
U 5B8E5B80
F0 "CANTester-v2-02-main" 50
F1 "CANTester-v2-02-main.sch" 50
$EndSheet
$Sheet
S 3350 4100 1200 700 
U 5B8E4E02
F0 "CANTester-v2-03-rs232" 50
F1 "CANTester-v2-03-rs232.sch" 50
$EndSheet
$Sheet
S 4700 4100 1200 700 
U 5B8E4D89
F0 "CANTester-v2-04-conn" 50
F1 "CANTester-v2-04-conn.sch" 50
$EndSheet
$EndSCHEMATC
