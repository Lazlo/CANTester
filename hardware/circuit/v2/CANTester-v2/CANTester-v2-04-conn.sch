EESchema Schematic File Version 4
LIBS:CANTester-v2-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J?
U 1 1 5B8E5071
P 4500 2300
F 0 "J?" H 4550 2717 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 4550 2626 50  0000 C CNN
F 2 "" H 4500 2300 50  0001 C CNN
F 3 "~" H 4500 2300 50  0001 C CNN
	1    4500 2300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J?
U 1 1 5B8E50B3
P 7950 3550
F 0 "J?" H 8000 3967 50  0000 C CNN
F 1 "CAN1" H 8000 3876 50  0000 C CNN
F 2 "" H 7950 3550 50  0001 C CNN
F 3 "~" H 7950 3550 50  0001 C CNN
	1    7950 3550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J?
U 1 1 5B8E50F3
P 7900 4450
F 0 "J?" H 7950 4867 50  0000 C CNN
F 1 "CAN2" H 7950 4776 50  0000 C CNN
F 2 "" H 7900 4450 50  0001 C CNN
F 3 "~" H 7900 4450 50  0001 C CNN
	1    7900 4450
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J?
U 1 1 5B8E5121
P 7150 3550
F 0 "J?" H 7200 3967 50  0000 C CNN
F 1 "SLCAN1" H 7200 3876 50  0000 C CNN
F 2 "" H 7150 3550 50  0001 C CNN
F 3 "~" H 7150 3550 50  0001 C CNN
	1    7150 3550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J?
U 1 1 5B8E5193
P 7150 4450
F 0 "J?" H 7200 4867 50  0000 C CNN
F 1 "SLCAN2" H 7200 4776 50  0000 C CNN
F 2 "" H 7150 4450 50  0001 C CNN
F 3 "~" H 7150 4450 50  0001 C CNN
	1    7150 4450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J?
U 1 1 5B8E5254
P 9700 3100
F 0 "J?" H 9800 3076 50  0000 L CNN
F 1 "CAN1_H" H 9800 2985 50  0000 L CNN
F 2 "" H 9700 3100 50  0001 C CNN
F 3 " ~" H 9700 3100 50  0001 C CNN
	1    9700 3100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J?
U 1 1 5B8E52E8
P 9700 3500
F 0 "J?" H 9800 3476 50  0000 L CNN
F 1 "CAN1_L" H 9800 3385 50  0000 L CNN
F 2 "" H 9700 3500 50  0001 C CNN
F 3 " ~" H 9700 3500 50  0001 C CNN
	1    9700 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J?
U 1 1 5B8E5314
P 9700 4150
F 0 "J?" H 9800 4126 50  0000 L CNN
F 1 "CAN2_H" H 9800 4035 50  0000 L CNN
F 2 "" H 9700 4150 50  0001 C CNN
F 3 " ~" H 9700 4150 50  0001 C CNN
	1    9700 4150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J?
U 1 1 5B8E534E
P 9700 4500
F 0 "J?" H 9800 4476 50  0000 L CNN
F 1 "CAN2_L" H 9800 4385 50  0000 L CNN
F 2 "" H 9700 4500 50  0001 C CNN
F 3 " ~" H 9700 4500 50  0001 C CNN
	1    9700 4500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
