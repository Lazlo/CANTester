EESchema Schematic File Version 4
LIBS:CANTester-v2-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Regulator_Linear:L7805 U?
U 1 1 5B8E598B
P 4650 2650
F 0 "U?" H 4650 2892 50  0000 C CNN
F 1 "L7805" H 4650 2801 50  0000 C CNN
F 2 "" H 4675 2500 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 4650 2600 50  0001 C CNN
	1    4650 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5B8E5A00
P 4000 2950
F 0 "C?" H 4115 2996 50  0000 L CNN
F 1 "C" H 4115 2905 50  0000 L CNN
F 2 "" H 4038 2800 50  0001 C CNN
F 3 "~" H 4000 2950 50  0001 C CNN
	1    4000 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 2650 4000 2650
Wire Wire Line
	4000 2800 4000 2650
Connection ~ 4000 2650
Wire Wire Line
	4000 2650 3400 2650
Wire Wire Line
	4650 2950 4650 3200
Wire Wire Line
	4000 3100 4000 3200
$Comp
L power:GND #PWR?
U 1 1 5B8E5AED
P 4000 3200
F 0 "#PWR?" H 4000 2950 50  0001 C CNN
F 1 "GND" H 4005 3027 50  0000 C CNN
F 2 "" H 4000 3200 50  0001 C CNN
F 3 "" H 4000 3200 50  0001 C CNN
	1    4000 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B8E5B03
P 4650 3200
F 0 "#PWR?" H 4650 2950 50  0001 C CNN
F 1 "GND" H 4655 3027 50  0000 C CNN
F 2 "" H 4650 3200 50  0001 C CNN
F 3 "" H 4650 3200 50  0001 C CNN
	1    4650 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 2650 5450 2650
$EndSCHEMATC
