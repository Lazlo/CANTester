EESchema Schematic File Version 4
LIBS:CANTester-v2-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Interface_UART:MAX232 U?
U 1 1 5B8E5C49
P 5950 5100
F 0 "U?" H 5950 6478 50  0000 C CNN
F 1 "MAX232" H 5950 6387 50  0000 C CNN
F 2 "" H 6000 4050 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/max232.pdf" H 5950 5200 50  0001 C CNN
	1    5950 5100
	1    0    0    -1  
$EndComp
$Comp
L Interface_UART:MAX232 U?
U 1 1 5B8E5C9B
P 2950 2450
F 0 "U?" H 2950 3828 50  0000 C CNN
F 1 "MAX232" H 2950 3737 50  0000 C CNN
F 2 "" H 3000 1400 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/max232.pdf" H 2950 2550 50  0001 C CNN
	1    2950 2450
	1    0    0    -1  
$EndComp
$Comp
L Interface_UART:MAX232 U?
U 1 1 5B8E5CE9
P 8750 2350
F 0 "U?" H 8750 3728 50  0000 C CNN
F 1 "MAX232" H 8750 3637 50  0000 C CNN
F 2 "" H 8800 1300 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/max232.pdf" H 8750 2450 50  0001 C CNN
	1    8750 2350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
