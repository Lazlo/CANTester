# CANTester v1 BOM

| Item                                             | Q | Price     |
| ------------------------------------------------ | - | --------- |
| _Active Components_                              |   |           |
| ATmega1284P                                      | 1 | EUR 5,15  |
| SJA1000 module with TJA1050                      | 1 | EUR 7,42  | 
| MAX232N                                          | 1 | EUR 0,39  |
| LM7805 (TO-220)                                  | 1 | EUR 0,24  |
| SSD1306 OLED                                     | 1 | EUR 7,49  |
| __Subtotal__                                     |   | EUR 20.69 |
| _Pasive Components_                              |   |           |
| 10k Ohm resistor (for ATmega reset)              | 1 |           |
| 120 Ohm resistor                                 | 1 |           |
| 100nF ceramic capacitor                          | 3 |           |
| 22pF ceramic capacitor                           | 2 |           |
| 1uF electrolytic capacitor (for MAX232)          | 4 |           |
| 100uF electrolytic capacitor (for LM7805)        | 1 |           |
| 16 MHz quarz (HC-49)                             | 1 |           |
| __Subtotal__                                     |   |           |
| _Mechanical Components_                          |   |           |
| barel socket                                     | 1 |           |
| DB9 socket male                                  | 1 |           |
| DB9 socket female                                | 1 |           |
| BNC socket                                       | 2 |           |
| two position switch                              | 1 |           |
| 40 pin dil socket                                | 1 |           |
| 16 pin dil socket                                | 1 |           |
| 4 port screw terminal                            | 1 |           |
| 8 pin header                                     | 4 |           |
| 2 pin header (vin, pwrswitch, can h/l, can\_res) | 4 |           |
| 6 pin header                                     | 1 |           |
| 6 pin header angled                              | 1 |           |
| 4 pin header                                     | 1 |           |
| 2 pin socket (female)                            | 5 |           |
| 4 pin socket (female)                            | 2 |           |
| 9x2 pin socket (female)                          | 1 |           |
| 10 pin ribbon cable                              | 2 |           |
| 4 pin ribbon cable                               | 1 |           |
| pair or red/black wires                          | 2 |           |
| pair of green/white wires                        | 1 |           |
| pair of red/red wires                            | 1 |           |
| antenna cable (copper core with shield)          | 2 |           |
| circuit board                                    | 1 |           |
| chassis                                          | 1 |           |
| __Subtotal__                                     |   |           |
| __Total__                                        |   |           |
